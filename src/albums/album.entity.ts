import { Artist } from "src/artists/artist.entity";
import { Song } from "src/songs/song.entity";
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Album extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    title: string;
    @Column()
    year: string;
    @Column()
    cover: string;

    @OneToMany(type => Song, song => song.id)
    songs: Song[];
    
    @ManyToOne(() => Artist, artist => artist.albums)
    artist : Artist;
}
