import { Optional } from '@nestjs/common';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsString, IsUrl } from 'class-validator';
import { url } from 'node:inspector';

export class CreateAlbumDto {
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    year: string;

    @IsString()
    @IsUrl()
    cover: string;

    @IsNotEmpty()
    artistId: number;

  }