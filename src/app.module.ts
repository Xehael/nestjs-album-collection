import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlbumsModule } from './albums/albums.module';
import { typeormConfig } from './config/typeorm.config';
import { ArtistsModule } from './artists/artists.module';
import { UserModule } from './user/user.module';
import { SongsModule } from './songs/songs.module';



@Module({
  imports: [AlbumsModule, TypeOrmModule.forRoot(typeormConfig), ArtistsModule, UserModule, SongsModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
