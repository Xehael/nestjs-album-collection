import { Optional } from "@nestjs/common";
import { IsNotEmpty } from "class-validator";
import { Album } from "src/albums/album.entity";

export class createArtistDto {
    @IsNotEmpty()
    name: string;
    @IsNotEmpty()
    isBand: boolean;
    @Optional()
    albums: Album
}