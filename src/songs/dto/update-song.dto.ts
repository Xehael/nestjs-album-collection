
import { IsNotEmpty, IsString} from 'class-validator';

export class UpdateSongDto {
    @IsNotEmpty()
    id: number
    @IsNotEmpty()
    @IsString()
    title: string;
    @IsNotEmpty()  
    duration: string;
    @IsNotEmpty()
    albumId: number;

  }