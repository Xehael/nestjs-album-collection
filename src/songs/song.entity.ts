import { Album } from "src/albums/album.entity";
import { BaseEntity, Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Song extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    title: string;
    @Column()
    duration: String;

    @ManyToOne(() => Album, album => album.songs)
    album: Album;
}