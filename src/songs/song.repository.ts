import { Album } from "src/albums/album.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateSongDto } from "./dto/create-song.dto";
import { Song } from "./song.entity";



@EntityRepository(Song)
export class SongRepository extends Repository<Song> {

    async createSong(createSongDto: CreateSongDto, album: Album): Promise<Song> {
        const {title, duration} =  createSongDto;

        const song = new Song();
        song.title = title;
        song.duration = duration;
        song.album = album;
        await song.save();

        return song;
    }

}