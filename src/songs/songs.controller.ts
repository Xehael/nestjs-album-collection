import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateSongDto } from './dto/create-song.dto';
import { UpdateSongDto } from './dto/update-song.dto';
import { Song } from './song.entity';
import { SongsService } from './songs.service';

@Controller('songs')
export class SongsController {
    constructor(private songsService: SongsService) {}

    @Get('getAllSongs')
    getAllSongs():Promise<Song[]>{
        console.log("GET ALL SONGS");
        return this.songsService.getAllSongs();
    }

    @Get('/:id')
    async getSongById(@Param('id', ParseIntPipe) id: number): Promise<Song> {
    return this.songsService.getSongById(id);
    }

    @Post('addSong')
    @UsePipes(ValidationPipe)
    addSong(@Body() createSongDto : CreateSongDto ){
    console.log("ADD SONG")
     if(createSongDto)
     {
      return this.songsService.addSong(createSongDto);
     }
    }

    @Patch('/updateSongs')
    @UsePipes(ValidationPipe)
    updateSongById(@Body() updateSongDto : UpdateSongDto): Promise<Song>{
        console.log("UPDATE SONG ")
        return this.songsService.updateSongById(updateSongDto);
    }

    @Delete('/:id')
    deleteSongById(@Param('id') id: number) : Promise<void> {
        console.log("DELETE SONG");
        return this.songsService.deleteSongById(id);
    }
}
