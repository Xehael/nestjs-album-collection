import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AlbumsModule } from 'src/albums/albums.module';
import { AlbumsService } from 'src/albums/albums.service';
import { SongRepository } from './song.repository';
import { SongsController } from './songs.controller';
import { SongsService } from './songs.service';

@Module({
    imports: [AlbumsModule,TypeOrmModule.forFeature([SongRepository])],
    controllers: [SongsController],
    providers: [SongsService]

})
export class SongsModule {}
