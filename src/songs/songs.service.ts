import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Song } from './Song.entity';
import { SongRepository } from './Song.repository';
import { CreateSongDto } from './dto/create-Song.dto';
import { v1 as uuid } from 'uuid';
import { UpdateSongDto } from './dto/update-Song.dto';
import { timer } from 'rxjs';
import { AlbumsService } from 'src/albums/albums.service';


@Injectable()
export class SongsService {

    constructor(
        @InjectRepository(SongRepository)
        private songRepository: SongRepository,
        private albumService: AlbumsService
    ) {}

    async getAllSongs(): Promise<Song[]> {
        const Songs = await this.songRepository.find({relations: ["album"]});
        if(!Songs){
            throw new NotFoundException("Song not found")
        }
        return Songs;
    }

    async getSongById(id: number): Promise<Song> {
        const found = await this.songRepository.findOne(id, {relations:["album"]});
        if (!found) {
          throw new NotFoundException(`Song with ID "${id}" not found`);
        }
        return found;
      }


    async addSong(createSong: CreateSongDto): Promise<Song>{
        const album = await this.albumService.getAlbumById(createSong.albumId);
          const insert = await this.songRepository.createSong(createSong, album);
          if(!insert){
              throw new Error("Song not inserted");
          }
         return insert;
    } 

    async updateSongById(updateSongDto : UpdateSongDto): Promise<Song>{
        const update : Song = await this.getSongById(updateSongDto.id)

        update.title = updateSongDto.title;
        update.duration = updateSongDto.duration;
        await update.save()
        return update;
    }

    async deleteSongById(id: number): Promise<void> {
        const taskdeleted = await this.songRepository.delete(id);
        if(taskdeleted.affected === 0){
            throw new NotFoundException(`Song ${id} not found`)
        }
    }


}
