import { MinLength, IsString ,IsNotEmpty, Matches, MaxLength} from 'class-validator'

export class UserCredentialsDto {
    @IsNotEmpty()
    @IsString()
    @MinLength(4)
    @MaxLength(30)
    mail: string
    @IsString()
    @MinLength(8)
    @MaxLength(20)
    @Matches(
      /((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/,
      { message: 'password too weak' },
    )
    password: string
}