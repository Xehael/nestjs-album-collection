
import { Controller , Post, Body, ValidationPipe} from '@nestjs/common'
import { UserService } from './user.service';
import { UserCredentialsDto } from './dto/user-credential.dto';

@Controller('user')
export class UserController {

    constructor(private userService : UserService) { }


    @Post('/signup')
    signUp(@Body(ValidationPipe) userCredentialsDto: UserCredentialsDto): Promise<void> {
      return this.userService.signUp(userCredentialsDto);
    }
  
    @Post('/signin')
    signIn(@Body(ValidationPipe) userCredentialsDto: UserCredentialsDto): Promise<{ accessToken: string }> {
      return this.userService.signIn(userCredentialsDto);
    }
}
