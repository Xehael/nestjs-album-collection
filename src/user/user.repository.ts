
import { User } from "./user.entity";
import * as bcrypt from 'bcrypt';
import { UserCredentialsDto } from "./dto/user-credential.dto";
import { InternalServerErrorException, ConflictException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";


@EntityRepository(User)
export class UserRepository extends Repository<User>{

    // async createArtist(createUserDto : CreateUserDto): Promise<User> {
    //     const {mail, password} =  createUserDto;

    //     const user = new User();
    //     user.mail = mail;
    //     user.password = password;
    //     await user.save();

    //     return user;

    // }


    async signUp(userCredentialsDto: UserCredentialsDto): Promise<void> {
        const { mail, password } = userCredentialsDto;
    
        const user = new User();
        user.mail = mail;
        user.salt = await bcrypt.genSalt();
        user.password = await this.hashPassword(password, user.salt);
    
        try {
          await user.save();
        } catch (error) {
          if (error.code === '23505') { // duplicate username
            throw new ConflictException('Username already exists');
          } else {
            throw new InternalServerErrorException();
          }
        }
      }
    
      async validateUserPassword(userCredentialsDto: UserCredentialsDto): Promise<string> {
        const { mail, password } = userCredentialsDto;
        const user = await this.findOne({ mail });
    
        if (user && await user.validatePassword(password)) {
          return user.mail;
        } else {
          return null;
        }
      }
    
      private async hashPassword(password: string, salt: string): Promise<string> {
        return bcrypt.hash(password, salt);
      }
    
    
}