import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from './user.repository';
import { JwtPayload } from './jwt-payload.interface';
import { UserCredentialsDto } from './dto/user-credential.dto';
import { UnauthorizedException } from '@nestjs/common';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,
    private jwtService: JwtService,
  ) {}

  async signUp(UserCredentialsDto: UserCredentialsDto): Promise<void> {
    return this.userRepository.signUp(UserCredentialsDto);
  }

  async signIn(UserCredentialsDto: UserCredentialsDto): Promise<{ accessToken: string }> {
    const mail = await this.userRepository.validateUserPassword(UserCredentialsDto);

    if (!mail) {
      throw new UnauthorizedException('Invalid credentials');
    }

    const payload: JwtPayload = { mail };
    const accessToken = await this.jwtService.sign(payload);

    return { accessToken };
  }
}
